package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null){
            throw new CannotBuildPyramidException();
        }
        int rows = 0, count = 0;
        while (count < inputNumbers.size()) {
            rows++;
            count += rows;
        }
        if (inputNumbers.size() != count) {
            throw new CannotBuildPyramidException();
        }
        int result[][];
        try {
            result = new int[rows][2 * rows - 1];
            Collections.sort(inputNumbers);
            Iterator numbersIterator = inputNumbers.iterator();
            for (int j = 0; j < rows; j++) {
                for (int i = 0; i < j + 1; i++) {
                    result[j][rows - 1 - j + i * 2] = numbersIterator.hasNext() ? (Integer) numbersIterator.next() : 0;
                }
            }
        } catch (NullPointerException | NegativeArraySizeException e){
            throw new CannotBuildPyramidException();
        }

        return result;
    }


}
