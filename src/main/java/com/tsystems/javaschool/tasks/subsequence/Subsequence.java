package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        int xPlace = -1, currPlace;
        boolean result = true;
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        for (Object aX : x) {
            currPlace = y.indexOf(aX);
            if (currPlace != -1 && xPlace < currPlace) {
                xPlace = currPlace;
            } else {
                result = false;
                break;
            }
        }
        return result;
    }
}
