package com.tsystems.javaschool.tasks.calculator;

import java.util.EmptyStackException;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        Double result = deem(parseStatement(statement));
        if (result == null) {
            return null;
        } else if ((double)result.intValue() == result) {
            return Integer.toString(result.intValue());
        } else {
            return result.toString();
        }
    }


    private boolean isDelem(char c) {
        return " ".indexOf(c) != -1;
    }

    private boolean isOperator(char c) {
        return "+-/*()".indexOf(c) != -1;
    }

    private int getPriority(char c) {
        switch (c) {
            case '(':
                return 0;
            case ')':
                return 1;
            case '+':
                return 2;
            case '-':
                return 3;
            case '*':
                return 4;
            case '/':
                return 4;
            default:
                return 5;
        }
    }

    private String parseStatement(String expression) {
        Stack<Character> stackForRPN = new Stack<>();
        StringBuilder result = new StringBuilder();
        char nextChar;
        StringBuilder nextNum = new StringBuilder();
        if (expression == null || expression.isEmpty()) {
            return null;
        }
        for (int i = 0; i < expression.length(); i++) {
            nextChar = expression.charAt(i);
            if (isDelem(nextChar)) {
                continue;
            }
            if (Character.isDigit(nextChar)) {
                while (!isDelem(nextChar) && !isOperator(nextChar)) {
                    nextNum.append(nextChar);
                    i++;
                    if (i == expression.length()) {
                        break;
                    } else {
                        nextChar = expression.charAt(i);
                    }
                }
                try {
                    result.append(Double.parseDouble(nextNum.toString()));
                    nextNum = new StringBuilder();
                } catch (NumberFormatException err) {
                    return null;
                }
                result.append(' ');
            }
            if (isOperator(nextChar)) {
                if (nextChar == '(') {
                    stackForRPN.push(nextChar);
                } else if (nextChar == ')') {
                    try {
                        char tempChar = stackForRPN.pop();
                        while (tempChar != '(') {
                            result.append(tempChar).append(' ');
                            tempChar = stackForRPN.pop();
                        }
                    } catch (EmptyStackException ese) {
                        return null;
                    }
                } else {
                    if (stackForRPN.size() > 0) {
                        if (getPriority(nextChar) <= getPriority(stackForRPN.peek())) {
                            result.append(stackForRPN.pop()).append(' ');
                        }
                    }
                    stackForRPN.push(nextChar);
                }
            }

        }
        while (stackForRPN.size() > 0) {
            char nextOperator = stackForRPN.pop();
            if (nextOperator == '(') return null;
            result.append(nextOperator).append(' ');
        }
        return result.toString();
    }

    private Double deem(String expression) {
        if (expression == null) {
            return null;
        }
        Double result;
        Stack<Double> stackTemp = new Stack<>();
        StringBuilder nextNum = new StringBuilder();
        char nextChar;
        for (int i = 0; i < expression.length(); i++) {
            nextChar = expression.charAt(i);
            if (Character.isDigit(nextChar)) {
                nextNum = new StringBuilder();
                while (!isDelem(nextChar) && !isOperator(nextChar)) {
                    nextNum.append(nextChar);
                    i++;
                    if (i == expression.length()) {
                        break;
                    } else {
                        nextChar = expression.charAt(i);
                    }
                }
                nextChar = expression.charAt(--i);
                stackTemp.push(Double.parseDouble(nextNum.toString()));
            } else if (isOperator(nextChar)) {
                try {
                    double b = stackTemp.pop();
                    double a = stackTemp.pop();
                    result = doOperation(a, b, nextChar);
                    if (result == null) {
                        return null;
                    }
                    stackTemp.push(result);
                } catch (EmptyStackException ese) {
                    return null;
                }
            }
        }
        return stackTemp.pop();
    }

    private Double doOperation(double a, double b, char operator) {
        switch (operator) {
            case '+':
                return a + b;
            case '-':
                return a - b;
            case '*':
                return a * b;
            case '/':
                return b == 0 ? null : a / b;
            default:
                return null;
        }
    }

}
